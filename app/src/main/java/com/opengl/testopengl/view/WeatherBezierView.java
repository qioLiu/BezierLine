package com.opengl.testopengl.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by six.sev on 2017/3/14.
 */

public class WeatherBezierView extends View  {
    //原始点
    private List<Point> mPoints = new ArrayList<Point>();
    //两个原始点的中点
    private List<Point> mMidPoints = new ArrayList<Point>();
    //两个中点的中点
    private List<Point> mMidMidPoints = new ArrayList<Point>();
    //辅助点
    private List<Point> mAssistPoints = new ArrayList<Point>();

    private int detaY = 200;
    private int screenWith;
    private int pointY = 500;

    private Paint mPaint;
    private Path mPath;

    public WeatherBezierView(Context context) {
        super(context);
        init(context);
    }

    public WeatherBezierView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPath = new Path();

        screenWith = context.getResources().getDisplayMetrics().widthPixels;

        //初始化原始点
        initPoints();
        //初始化两点之间的中点
        initMidPoints();
        //初始化中点的中点
        initMidMidPoints();
        //初始化辅助点
        initAssistPoints();
    }

    private void initPoints() {
        int width = screenWith / 6;
        for (int i = 0; i < 5; i++) {
            Point point = new Point();
            point.x = (int) (width * (i + 0.5));
            if(i % 2 == 0){
                point.y = pointY;
            }else{
                point.y = pointY - detaY;
            }
            mPoints.add(point);
        }
    }

    private void initMidPoints() {
        if(mPoints.size() <= 0){
            return;
        }
        for (int i = 0; i < mPoints.size(); i++) {
            if(i == mPoints.size() - 1){
                break;
            }
            Point point = new Point();
            point.x = (mPoints.get(i).x + mPoints.get(i + 1).x) / 2;
            point.y = (mPoints.get(i).y + mPoints.get(i + 1).y) / 2;
            mMidPoints.add(point);
        }
    }

    private void initMidMidPoints() {
        if(mMidPoints.size() <= 0){
            return;
        }
        for (int i = 0; i < mMidPoints.size(); i++) {
            if(i == mMidPoints.size() - 1){
                break;
            }
            Point point = new Point();
            point.x = (mMidPoints.get(i).x + mMidPoints.get(i + 1).x) / 2;
            point.y = (mMidPoints.get(i).y + mMidPoints.get(i + 1).y) / 2;
            mMidMidPoints.add(point);
        }
    }

    private void initAssistPoints(){
        for (int i = 0; i < mPoints.size(); i++) {
            if(i == 0 || i == mPoints.size() - 1){
                continue;
            }
            Point point1 = new Point();
            Point point2 = new Point();
            point1.x = mPoints.get(i).x - mMidMidPoints.get(i - 1). x + mMidPoints.get(i - 1).x;
            point1.y = mPoints.get(i).y - mMidMidPoints.get(i - 1). y + mMidPoints.get(i - 1).y;
            mAssistPoints.add(point1);
            point2.x = mMidPoints.get(i).x - mMidMidPoints.get(i - 1).x + mPoints.get(i).x;
            point2.y = mMidPoints.get(i).y - mMidMidPoints.get(i - 1).y + mPoints.get(i).y;
            mAssistPoints.add(point2);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawPointLine(canvas);
        drawMidPoint(canvas);
        drawMidMidPoint(canvas);
        drawAssistPoint(canvas);
        drawBezier(canvas);
    }

    private void drawBezier(Canvas canvas) {
        mPath.reset();
        mPaint.setStrokeWidth(2);
        mPaint.setColor(Color.BLUE);
        for (int i = 0; i < mPoints.size(); i++) {
            if (i == 0){ //第一个和第二个点之间是二阶贝塞尔
                mPath.moveTo(mPoints.get(i).x, mPoints.get(i).y);
                mPath.quadTo(mAssistPoints.get(i).x, mAssistPoints.get(i).y, mPoints.get(i + 1).x, mPoints.get(i + 1).y);
            }else if(i == mPoints.size() - 2){//倒数第二个点和最后一个点之间也是二阶贝塞尔
                mPath.moveTo(mPoints.get(i).x, mPoints.get(i).y);
                mPath.quadTo(mAssistPoints.get(mAssistPoints.size() - 1).x, mAssistPoints.get(mAssistPoints.size() - 1).y, mPoints.get(i + 1).x, mPoints.get(i + 1).y);
            }else if(i < mPoints.size() - 2){ //中间的都为三阶贝塞尔
                mPath.cubicTo(mAssistPoints.get(i*2 - 1).x, mAssistPoints.get(i*2 -1).y, mAssistPoints.get(i*2).x, mAssistPoints.get(i*2 -1).y, mPoints.get(i + 1).x, mPoints.get(i+ 1).y);
            }
        }
        canvas.drawPath(mPath, mPaint);
    }

    private void drawAssistPoint(Canvas canvas) {
        mPaint.setColor(Color.GRAY);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(8);
        mPath.reset();
        mPath.moveTo(mAssistPoints.get(0).x, mAssistPoints.get(0).y);
        for (int i = 0; i < mAssistPoints.size(); i++) {
            canvas.drawPoint(mAssistPoints.get(i).x, mAssistPoints.get(i).y,mPaint);
        }
    }

    private void drawMidMidPoint(Canvas canvas) {
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(8);
        mPath.reset();
        mPath.moveTo(mMidMidPoints.get(0).x, mMidMidPoints.get(0).y);
        for (int i = 0; i < mMidMidPoints.size(); i++) {
            canvas.drawPoint(mMidMidPoints.get(i).x, mMidMidPoints.get(i).y,mPaint);
        }
    }

    private void drawMidPoint(Canvas canvas) {
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(8);
        mPath.reset();
        mPath.moveTo(mMidPoints.get(0).x, mMidPoints.get(0).y);
        for (int i = 0; i < mMidPoints.size(); i++) {
            canvas.drawPoint(mMidPoints.get(i).x, mMidPoints.get(i).y,mPaint);
        }
        /*mPaint.setColor(Color.GREEN);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(2);
        for (int i = 0; i < mMidPoints.size(); i++) {
            mPath.lineTo(mMidPoints.get(i).x, mMidPoints.get(i).y);
        }
        canvas.drawPath(mPath, mPaint);*/
    }

    private void drawPointLine(Canvas canvas) {
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(8);
        mPath.reset();
        mPath.moveTo(mPoints.get(0).x, mPoints.get(0).y);
        for (int i = 0; i < mPoints.size(); i++) {
            canvas.drawPoint(mPoints.get(i).x, mPoints.get(i).y,mPaint);
        }
        mPaint.setColor(Color.RED);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(2);
        for (int i = 0; i < mPoints.size(); i++) {
            mPath.lineTo(mPoints.get(i).x, mPoints.get(i).y);
        }
        canvas.drawPath(mPath, mPaint);
    }
}
