package com.opengl.testopengl.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.opengl.testopengl.R;

/**
 * 贝塞尔曲线
 * Created by six.sev on 2017/3/14.
 */

public class BezierView extends View {
    //画笔
    private Paint mPaint;
    //路径
    private Path mPath;
    //起始点X
    private int start_x;
    //起始点Y
    private int start_y;
    //若需要画三阶贝塞尔需要增加 一个辅助点
    //辅助点X
    private int assist_x;
    //辅助点Y
    private int assist_y;
    //结束点X
    private int end_x;
    //结束点Y
    private int end_y;

    public BezierView(Context context) {
        this(context, null);
    }

    public BezierView(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.BezierView);
        start_x = array.getInteger(R.styleable.BezierView_start_x, 100);
        start_y = array.getInteger(R.styleable.BezierView_start_y, 100);
        assist_x = array.getInteger(R.styleable.BezierView_assist_x, 100);
        assist_y = array.getInteger(R.styleable.BezierView_assist_y, 100);
        end_x = array.getInteger(R.styleable.BezierView_end_x, 100);
        end_y = array.getInteger(R.styleable.BezierView_end_y, 100);
        array.recycle();

        mPaint = new Paint();
        mPath = new Path();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setColor(Color.RED);
        mPaint.setAntiAlias(true);
        //重置路径
        mPath.reset();
        mPath.moveTo(start_x, start_y);
        //二阶贝塞尔    三阶使用clubTo
        mPath.quadTo(assist_x, assist_y, end_x, end_y);
        canvas.drawPath(mPath, mPaint);

        mPaint.setColor(Color.BLACK);
        canvas.drawPoint(start_x, start_y, mPaint);
        canvas.drawPoint(assist_x, assist_y, mPaint);
        canvas.drawPoint(end_x, end_y, mPaint);
        canvas.drawLine(start_x, start_y, assist_x, assist_y, mPaint);
        canvas.drawLine(assist_x, assist_y, end_x , end_y, mPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()){
            case MotionEvent.ACTION_MOVE:
                assist_x = (int) event.getX();
                assist_y = (int) event.getY();
                invalidate();
                break;
        }
        return true;
    }
}
